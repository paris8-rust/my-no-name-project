# Petit Jeu Breakout

J'ai voulu tenter de créer un Breakout à ma façon après avoir vu celui que Bevy proposait en exemple : https://github.com/bevyengine/bevy/blob/master/examples/game/breakout.rs

Je pensais que j'aurais pu me baser sur un autre jeu qui aurait un méchanisme similaire, dont le tutoriel est ici : https://mbuffett.com/posts/bevy-snake-tutorial/ , mais la façon dont il a incorporé son " Snake " avec un "system::Stage" m'a posé problème, et j'ai du m'inspirer plus du Breakout que Bevy proposait au final. Je pensais que j'aurais pu compenser en ajoutant de meilleurs fonctions, tel que certaines briques se brise seulement après avoir été touché plusieurs fois, ou une balle qui change de vitesse plus l'on joue, ou même deux balles a la fois, mais je ne pensais pas que j'aurais eu tant de mal. J'essayerais quand même d'incorporer ça si j'y arrive..
