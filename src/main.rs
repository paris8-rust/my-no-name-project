use bevy::prelude::*;
use std::thread::spawn;
use bevy::render::pass::ClearColor;
use bevy::sprite::collide_aabb::{collide, Collision};
use bevy::ecs::{Commands, Query, Res};



fn main() {
    App::build()
        .add_startup_system(setup.system())
        .add_resource(ClearColor(Color::rgb(0.8, 0.8, 0.8)))
        .add_startup_stage("game_setup", SystemStage::single(spawn_ball.system()))
        .add_system(paddle_move.system())
        .add_system(ball_movement.system())
        .add_system(ball_collision.system())
        .add_event::<GameOver>()
        .add_plugins(DefaultPlugins)
        .run();
}

struct Paddle{
    speed: f32,
}
struct Ball{
    velocity: Vec3,
}

struct GameOver;

struct Materials {
    ball_materials: Handle<ColorMaterial>,
}

enum Collider {
    Solid,
    Brick,
    Paddle,
}

fn setup(commands: &mut Commands, mut materials: ResMut<Assets<ColorMaterial>>) {
    commands.spawn(Camera2dBundle::default());
    commands.insert_resource(Materials{
        ball_materials: materials.add(Color::rgb(1.0,1.0,1.0).into()),
    });

    // Paddle spawn
    let paddle_material = materials.add(Color::rgb(0.3, 0.3, 0.3).into());
    commands
        .spawn(SpriteBundle{
            material: paddle_material.clone(),
            sprite: Sprite::new(Vec2::new(120.0,30.0)),
            transform: Transform::from_translation(Vec3::new(0.0, -300.0, 0.0)),
            ..Default::default()
        })
        .with(Paddle{speed: 10.0})
        .with(Collider:: Paddle);

    // Making the walls
    let wall_material = materials.add(Color::rgb(0.3,0.3, 0.3).into());
    let wall_thick = 10.0;
    let bounds = Vec2::new(1270.0, 800.0);
    commands
        // Left wall
        .spawn(SpriteBundle{
            material: wall_material.clone(),
            sprite: Sprite::new(Vec2::new(wall_thick, bounds.y + wall_thick)),
            transform: Transform::from_translation(Vec3::new(-bounds.x /2.0, 0.0, 0.0)),
            ..Default::default()
        })
        .with(Collider::Solid)
        // Right wall
        .spawn(SpriteBundle{
            material: wall_material.clone(),
            sprite: Sprite::new(Vec2::new(wall_thick, bounds.y + wall_thick)),
            transform: Transform::from_translation(Vec3::new(bounds.x / 2.0, 0.0, 0.0)),
            ..Default::default()
        })
        .with(Collider::Solid)
        //Top Wall
        .spawn(SpriteBundle{
            material: wall_material.clone(),
            sprite: Sprite::new(Vec2::new(bounds.x, wall_thick)),
            transform: Transform::from_translation(Vec3::new( 0.0, 355.0, 0.0)),
            ..Default::default()
        })
        .with(Collider::Solid);

    // Bricks spawn
    let brick_rows = 6;
    let brick_columns = 10;
    let brick_spacing = 15.0;
    let brick_size = Vec2::new(100.0, 30.0);
    let bricks_width = brick_columns as f32 * (brick_size.x + brick_spacing) - brick_spacing;
    // Position of the bricks
    let bricks_offset = Vec3::new(-(bricks_width - brick_size.x) / 2.0, 100.0, 0.0);
    let brick_material = materials.add(Color::rgb(0.5, 0.2, 0.2).into());
    for row in 0..brick_rows {
        let y_position = row as f32 * (brick_size.y + brick_spacing);
        for column in 0..brick_columns {
            let brick_position = Vec3::new(
                column as f32 * (brick_size.x + brick_spacing),
                y_position,
                0.0,
            ) + bricks_offset;
            commands
                // brick
                .spawn(SpriteBundle {
                    material: brick_material.clone(),
                    sprite: Sprite::new(brick_size),
                    transform: Transform::from_translation(brick_position),
                    ..Default::default()
                })
                .with(Collider::Brick);
        }
    }
}

// Ball Spawn
fn spawn_ball(commands: &mut Commands, materials:Res<Materials>){
    commands
        .spawn(SpriteBundle{
            material: materials.ball_materials.clone(),
            transform: Transform::from_translation(Vec3::new(0.0, -210.0, 1.0)),
            sprite:Sprite::new(Vec2::new(35.0,30.0)),
            ..Default::default()
        })
        .with(Ball {
            velocity: 7.0 * Vec3::new(0.5, -0.5, 0.0).normalize(),
        });
}

// Paddle Movement
fn paddle_move(
    keyboard_input: Res<Input<KeyCode>>,
    mut paddle_pos: Query<(&Paddle, &mut Transform)>,
) {
    for (paddle, mut transform) in paddle_pos.iter_mut() {
        let mut direction = 0.0;
        if keyboard_input.pressed(KeyCode::Q) {
            direction -= 1.0;
        }

        if keyboard_input.pressed(KeyCode::D) {
            direction += 1.0;
        }
        let translation = &mut transform.translation;
        translation.x += direction * paddle.speed;
        // bound the paddle within the walls
        translation.x = translation.x.min(560.0).max(-560.0);
    }
}

// Ball Movement
fn ball_movement(mut game_over_events: ResMut<Events<GameOver>>,
                 mut ball_pos: Query<(&Ball, &mut Transform)>) {
    for (ball, mut transform) in ball_pos.iter_mut() {
        transform.translation += ball.velocity;
    }
}

fn ball_collision(
    commands: &mut Commands,
    mut ball_pos: Query<(&mut Ball, &Transform, &Sprite)>,
    collider_pos: Query<(Entity, &Collider, &Transform, &Sprite)>,
){
    let mut counter = 1;
    for (mut ball, ball_transform, sprite) in ball_pos.iter_mut() {
        let ball_size = sprite.size;
        let velocity = &mut ball.velocity;


        for (collider_entity, collider,transform, sprite) in collider_pos.iter() {
            let collision = collide(
                ball_transform.translation,
                ball_size,
                transform.translation,
                sprite.size,
            );
            if let Some(collision) = collision {
                if let Collider::Brick = *collider {
                    counter += 1;
                    commands.despawn(collider_entity);

                }
                let mut reflect_x = false;
                let mut reflect_y = false;

                match collision {
                    Collision::Left => reflect_x = velocity.x > 0.0,
                    Collision::Right => reflect_x = velocity.x < 0.0,
                    Collision::Top => reflect_y = velocity.y < 0.0,
                    Collision::Bottom => reflect_y = velocity.y > 0.0,
                }

                if reflect_x {
                    velocity.x = -velocity.x;
                }
                if reflect_y {
                    velocity.y = -velocity.y;
                }
                if let Collider::Solid = *collider {
                    break;
                }
            }
        }
    }
}

